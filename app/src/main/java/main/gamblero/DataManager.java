package main.gamblero;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DataManager {


    private static String TAG ="DataManager";
    //---------------
    //Attributes
    //---------------

    private FirebaseDatabase database;
    private DatabaseReference myRef;
    ValueEventListener userlistener;
    User tempUser;

    //-------------
    //Constructor
    //-------------
    public DataManager()
    {

        database = FirebaseDatabase.getInstance();


        userlistener = new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                User user = dataSnapshot.getValue(User.class);
                if (user==null)
                {
                    Log.i(TAG,"User not found");
                    writeNewUser();
                }
                else
                {
                    Log.i(TAG,"Usuario Found");

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        };

    }



    public void findUser (String userId)
    {

        myRef = database.getReference("Users/"+userId);
        myRef.addValueEventListener(userlistener);


    }

    public void writeNewUser()
    {

            myRef = database.getReference("Users");
            myRef.child(tempUser.getID()).setValue(tempUser);
            Log.i(TAG, "Created User: " + tempUser.getID());

    }

    public void authenticateUser(String userId, String name, String location, float credit, String mail, String phoneNumber)
    {

        tempUser =  new User(userId,name, location, credit, mail, phoneNumber);
        findUser(userId);


    }




}
