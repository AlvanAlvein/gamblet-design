package main.gamblero;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Button;


public class invite_friends extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //Opening Dialogue Box
    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        //Opening Dialog Box
        myDialog = new Dialog(this);

        //Spinner starts here

        Spinner spinner = findViewById(R.id.friendspinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.friends, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    public void ShowPopUp(View v) {
        Button LogOut;
        //Shows the selected activity
        myDialog.setContentView(R.layout.activity_pop_up);
        LogOut = (Button) myDialog.findViewById(R.id.EnterRoom);


        LogOut.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                myDialog.dismiss();
            }

        });
        myDialog.show();


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),text, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {





    }


}
