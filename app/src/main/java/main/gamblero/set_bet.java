package main.gamblero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.SeekBar;
import android.view.View;
import android.content.Intent;

public class set_bet extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView textView;

    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set__bet);

        // Modify Seekbar
        textView = (TextView) findViewById(R.id.betNumber);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        //Get changes  from seekBar

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText("" + progress + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });






        //Initialize button

            button = findViewById(R.id.submit);
            button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                game_meny();
            }
        });
    }
        public void game_meny(){

            Intent intent = new Intent (this, game_meny.class);
            startActivity(intent);
        }

    }


