package main.gamblero;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LoginActivity";

    //Variable that uses the Google API to provide google based authentication
    private GoogleApiClient googleApiClient;

    private DataManager dataManager;

    //Sign in button provided by Google API
    private SignInButton signInButton;
    private ProgressBar progressBar;

    //Authentication with Firebase
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListner;


    public static final int SIGN_IN_CODE = 777;


    //------------------------------
    //SB: Activity lifecycle Methods
    //------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG,"Starting Login Activity \n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.initGoogleApi();
        this.initFirebaseApi();
        this.initInterface();
        this.dataManager = new DataManager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListner);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuthListner != null)
        {
            firebaseAuth.removeAuthStateListener(firebaseAuthListner);
        }
    }

    //-------------------------------------------------------------------------

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //This method manages the result of the Authentication Process
    //from the google API.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //The code is to identify from which specific process
        //the requeste came from

        if (requestCode == SIGN_IN_CODE)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess())
        {
            Toast.makeText(this, "User Authenticated", Toast.LENGTH_SHORT).show();
            firebaseAuthwithGoogle(result.getSignInAccount());
        }

        else
            {

                Toast.makeText(this, "Authentication Failed", Toast.LENGTH_SHORT).show();
            }
    }

    private void firebaseAuthwithGoogle(GoogleSignInAccount signInAccount)
    {
        progressBar.setVisibility(View.VISIBLE);
        signInButton.setVisibility(View.GONE);


        AuthCredential credential = GoogleAuthProvider.getCredential(signInAccount.getIdToken(),null);


        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(!task.isSuccessful())
                {
                    progressBar.setVisibility(View.GONE);
                    signInButton.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),"Firebase Authentication Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void goMainScreen()
    {
        Intent intent = new Intent(this,MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void initGoogleApi()
    {
        //Signs in a user an requests its email
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Call the API to get the Authentication Services
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso).build();

    }

    public void initFirebaseApi()
    {
        //Initializing the Firebase API
        firebaseAuth =  FirebaseAuth.getInstance();

        //Whenever there are changes in the authentication state of
        //firebase, this method gets called.
        firebaseAuthListner = new FirebaseAuth.AuthStateListener() {

            //This method listens whenever the users is authenticated or not.
            //It is executed when the authentication state changes
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user!= null)
                {
                    dataManager.authenticateUser(user.getUid(), user.getDisplayName(), "", 0, user.getEmail(), user.getPhoneNumber());
                    goMainScreen();

                }

            }
        };

    }

    private void initInterface()
    {

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //Set the controls when using the Sign In Button

        signInButton = findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,SIGN_IN_CODE);
            }
        });
    }


}
