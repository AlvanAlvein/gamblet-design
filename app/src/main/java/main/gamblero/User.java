package main.gamblero;

public class User {


    //--------------------
    //Attributes
    //-------------------
    private String name;
    private String location;
    private float credit;
    private String mail;
    private String phoneNumber;
    private String ID;

    //--------------
    //Constructor
    //-------------

    public User(){

    }



    public User(String ID, String name, String location, float credit, String mail, String phoneNumber)
    {
        this.setName(name);
        this.setLocation(location);
        this.setCredit(credit);
        this.setMail(mail);
        this.setPhoneNumber(phoneNumber);
        this.setID(ID);

    }

    //--------------------
    //Setters and Getters
    //--------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public String getPhoneNumber() { return phoneNumber;  }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber;  }

    public String getMail() { return mail; }

    public void setMail(String mail) { this.mail = mail; }

    public String getID() {    return ID;   }

    public void setID(String ID) {  this.ID = ID;  }
}
